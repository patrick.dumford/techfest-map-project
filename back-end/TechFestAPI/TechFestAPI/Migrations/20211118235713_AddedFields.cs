﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace TechFestAPI.Migrations
{
    public partial class AddedFields : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "LocationName",
                table: "Student",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Address",
                table: "RsiCenter",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "CityState",
                table: "RsiCenter",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "PhoneNumber",
                table: "RsiCenter",
                type: "nvarchar(max)",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "LocationName",
                table: "Student");

            migrationBuilder.DropColumn(
                name: "Address",
                table: "RsiCenter");

            migrationBuilder.DropColumn(
                name: "CityState",
                table: "RsiCenter");

            migrationBuilder.DropColumn(
                name: "PhoneNumber",
                table: "RsiCenter");
        }
    }
}
