﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using TechFestAPI.Models;

namespace TechFestAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class RsiCentersController : ControllerBase
    {
        private readonly TechFestDbContext _context;

        public RsiCentersController(TechFestDbContext context)
        {
            _context = context;
        }

        // GET: api/RsiCenters
        [HttpGet]
        public async Task<ActionResult<IEnumerable<RsiCenter>>> GetRsiCenter()
        {
            return await _context.RsiCenters.ToListAsync();
        }

        // GET: api/RsiCenters/5
        [HttpGet("{id}")]
        public async Task<ActionResult<RsiCenter>> GetRsiCenter(int id)
        {
            var rsiCenter = await _context.RsiCenters.FindAsync(id);

            if (rsiCenter == null)
            {
                return NotFound();
            }

            return rsiCenter;
        }

        // PUT: api/RsiCenters/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutRsiCenter(int id, RsiCenter rsiCenter)
        {
            if (id != rsiCenter.CenterId)
            {
                return BadRequest();
            }

            _context.Entry(rsiCenter).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!RsiCenterExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/RsiCenters
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<ActionResult<RsiCenter>> PostRsiCenter(RsiCenter rsiCenter)
        {
            _context.RsiCenters.Add(rsiCenter);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetRsiCenter", new { id = rsiCenter.CenterId }, rsiCenter);
        }

        // DELETE: api/RsiCenters/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteRsiCenter(int id)
        {
            var rsiCenter = await _context.RsiCenters.FindAsync(id);
            if (rsiCenter == null)
            {
                return NotFound();
            }

            _context.RsiCenters.Remove(rsiCenter);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        private bool RsiCenterExists(int id)
        {
            return _context.RsiCenters.Any(e => e.CenterId == id);
        }
    }
}
