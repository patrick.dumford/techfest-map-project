﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using TechFestAPI.Models;

namespace TechFestAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class StudentsController : ControllerBase
    {
        private readonly TechFestDbContext _context;

        public StudentsController(TechFestDbContext context)
        {
            _context = context;
        }

        // GET: api/Students
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Student>>> GetRSICenters()
        {
            return await _context.Students.ToListAsync();
        }

        // GET: api/Students/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Student>> GetStudent(int id)
        {
            var student = await _context.Students.FindAsync(id);

            if (student == null)
            {
                return NotFound();
            }

            return student;
        }

        [HttpGet("StudentByLocation/{lat}/{lng}")]
        public async Task<ActionResult<Student>> GetStudentByLocation(decimal lat, decimal lng)
        {
            var student = await _context.Students.Where(x => x.lat == lat && x.lng == lng).FirstOrDefaultAsync();

            return student;
        }

        // GET: api/Students/GetSchoolCount
        [HttpGet("SchoolCount/{lat}/{lng}")]
        public async Task<ActionResult<IEnumerable<SchoolCount>>> GetSchoolCountByLocation(decimal lat, decimal lng)
        {
            List<SchoolCount> SchoolCounts = new List<SchoolCount>();

            var students = await _context.Students.Where(x => x.lat == lat && x.lng == lng).ToListAsync();

            foreach (var student in students)
            {
                if (SchoolCounts.Count() == 0)
                {
                    SchoolCounts.Add(new SchoolCount(student.School, 1));
                }
                else
                {
                    bool exists = false;

                    foreach (var school in SchoolCounts.ToList())
                    {
                        if (school.SchoolName == student.School)
                        {
                            exists = true;
                            school.Count += 1;
                        }
                        
                    }

                    if (!exists)
                    {
                        SchoolCounts.Add(new SchoolCount(student.School, 1));
                    }
                }  
            }
            

            return SchoolCounts;
        }

        // PUT: api/Students/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutStudent(int id, Student student)
        {
            if (id != student.StudentId)
            {
                return BadRequest();
            }

            _context.Entry(student).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!StudentExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Students
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<ActionResult<Student>> PostStudent(Student student)
        {
            _context.Students.Add(student);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetStudent", new { id = student.StudentId }, student);
        }

        // DELETE: api/Students/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteStudent(int id)
        {
            var student = await _context.Students.FindAsync(id);
            if (student == null)
            {
                return NotFound();
            }

            _context.Students.Remove(student);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        private bool StudentExists(int id)
        {
            return _context.Students.Any(e => e.StudentId == id);
        }
    }
}
