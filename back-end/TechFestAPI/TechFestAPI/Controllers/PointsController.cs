﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using TechFestAPI.Models;

namespace TechFestAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PointsController : ControllerBase
    {
        private readonly TechFestDbContext _context;

        public PointsController(TechFestDbContext context)
        {
            _context = context;
        }


        // GET: api/Points/UniquePoints
        [HttpGet("UniquePoints")]
        public async Task<ActionResult<IEnumerable<Point>>> GetUniquePoints()
        {
            List<Point> points = new List<Point>();

            var AllRsiCenters = await _context.RsiCenters.ToListAsync();
                
            foreach (var center in AllRsiCenters)
            {
                var Rsipoint = new Point(0, center.CenterId, center.CityState, center.lng, center.lat);
                points.Add(Rsipoint);
            }

            var AllStudents = await _context.Students.ToListAsync();

            foreach (var student in AllStudents)
            {
                var Studentpoint = new Point(student.StudentId, 0, student.LocationName, student.lng, student.lat);
                points.Add(Studentpoint);
            }

            return points;
        }


        // GET: api/Points/NearestCenter
        [HttpGet("NearestCenter/{lat}/{lng}")]
        public async Task<ActionResult<RsiCenter>> GetNearestCenter(decimal lat, decimal lng)
        {
            var RsiCenters = await _context.RsiCenters.ToListAsync();

            var closestCenter = new RsiCenter();

            // Not the best way of doing this ik
            double closestDistance = 10000000;

            foreach (var rsiCenter in RsiCenters)
            {
                //lat = a
                var a = Math.Abs(rsiCenter.lat - lat);

                //lng = b
                var b = Math.Abs(rsiCenter.lng - lng);


                var distance = (Math.Sqrt(decimal.ToDouble(a) * decimal.ToDouble(a) + decimal.ToDouble(b) * decimal.ToDouble(b)));

                if (distance < closestDistance)
                {
                    closestCenter = rsiCenter;
                    closestDistance = distance;
                }
            }

            return closestCenter;
        }

    }
}
