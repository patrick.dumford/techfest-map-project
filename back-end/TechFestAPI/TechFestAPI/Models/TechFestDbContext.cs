﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using TechFestAPI.Models;

namespace TechFestAPI.Models
{
    public class TechFestDbContext : DbContext
    {
        public TechFestDbContext(DbContextOptions<TechFestDbContext> options) : base(options)
        {

        }

        public DbSet<Student> Students { get; set; }
        public DbSet<RsiCenter> RsiCenters { get; set; }
        public DbSet<School> Schools { get; set; }
    }
}
