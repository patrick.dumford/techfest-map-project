﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace TechFestAPI.Models
{
    public class Student
    {
        [Key]
        public int StudentId { get; set; }
        public string School { get; set; }
        public string LocationName { get; set; }
        public decimal lat { get; set; }
        public decimal lng { get; set; }

    }
}
