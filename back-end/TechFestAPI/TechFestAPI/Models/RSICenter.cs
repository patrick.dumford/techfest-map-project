﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace TechFestAPI.Models
{
    public class RsiCenter
    {
        [Key]
        public int CenterId { get; set; }
        public string Name { get; set; }
        public string PhoneNumber { get; set; }
        public string Address { get; set; }
        public string CityState { get; set; }
        public string ImageUrl { get; set; }
        public decimal lat { get; set; }
        public decimal lng { get; set; }
    }
}
