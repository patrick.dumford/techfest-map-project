﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TechFestAPI.Models
{
    public class SchoolCount
    {
        public SchoolCount(string schoolName, int count)
        {
            SchoolName = schoolName;
            Count = count;
        }

        public string SchoolName { get; set; }
        public int Count { get; set; }

    }
}
