﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace TechFestAPI.Models
{
    public class Point
    {
        public Point(int studentId, int centerId, string locationName, decimal lng, decimal lat)
        {
            StudentId = studentId;
            CenterId = centerId;
            LocationName = locationName;
            Lng = lng;
            Lat = lat;
        }

        public int StudentId { get; set; }
        public int CenterId { get; set; }
        public string LocationName { get; set; }
        public decimal Lng { get; set; }
        public decimal Lat { get; set; }
    }
}