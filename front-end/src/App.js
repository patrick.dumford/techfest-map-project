import './App.css';
import React from "react";
import logo from "./logo.svg";
import Map from "./components/map/map"
import Modal from "./components/Modal/Modal"
import SearchBar from "./components/Search/SearchBar"
import Button from "./components/Button/Button"
import CenterSideCard from "./components/SideCard/CenterSideCard"
import StudentSideCard from "./components/SideCard/StudentSideCard"
import axios from "axios";
import PlacesAutocomplete, {
  geocodeByAddress,
  getLatLng,
} from 'react-places-autocomplete';

function App() {
  const [data, setData] = React.useState(null);

  const [RSICenters, setRSICenters] = React.useState([]);
  const [students, setStudents] = React.useState([]);
  const [points, setPoints] = React.useState([]);
  
  const [autocomplete, setAutocomplete] = React.useState(null);
  const [isLoading, setIsLoading] = React.useState(false);

  // bool val is temperary, it will be something like pointType: RSI or Student, Lat: 0, Lng: 0
  const [selectedPoint, setSelectedPoint] = React.useState(null);
  const [searchValue, setSearchValue] = React.useState({});
  const [address, setAddress] = React.useState('');
  const [center, setCenter] = React.useState({ lat: 40.3595573680111, lng: -99.45250552767216 });
  const [zoom, setZoom] = React.useState(4);
  const [student, setStudent] = React.useState({ school: "", lat: null, lng: null });
  const [modal, setModal] = React.useState(false);

  const [selectedCenterId, setSelectedCenterId] = React.useState();

  const addStudent = (schoolName) => {   
    axios.post("https://localhost:44326/api/Students", {
      studentId: 0,
      school: schoolName,
      LocationName: address,
      lat: searchValue.lat,
      lng: searchValue.lng
    })
    .then((res) => {
      console.log(res.data);
    })
    .catch(function (error) {
      console.log(error);
    });

    setModal(false);
    setZoom(10);
    setCenter(searchValue);
    setSelectedPoint({ lat: searchValue.lat, lng: searchValue.lng});
    
  }


  React.useEffect(() => {
    axios
      .get("https://localhost:44326/api/Points/UniquePoints")
      .then((res) => setPoints(res.data))
      .catch(function (error) {
        console.log(error);
    });

  },[{modal}]);


  

  return (
    <div className="App">
      {selectedPoint && < StudentSideCard selectedPoint={ selectedPoint }/>}
      {selectedCenterId && <CenterSideCard CenterId={ selectedCenterId }/> }

      {(selectedPoint == null && selectedCenterId == null) && 
        <div>
          <SearchBar setCenter={ setCenter } setSearch={ setSearchValue } setAddress={ setAddress }/>
          <Button setModal={ setModal }/>
        </div>
      }
      
      
      <Map points={ points } coords={ center } zoom={ zoom } setSelectedPoint={ setSelectedPoint } setSelectedCenterId={ setSelectedCenterId }/>

      { modal && < Modal setModal={ setModal } addStudent={ addStudent }/> }
    </div>
    
  );
}

export default App;

{/* <div className="App">
<Map />
</div> */}