import './Map.css';
import React from 'react';
import GoogleMapReact from 'google-map-react';

import { Paper, Typography, useMediaQuery } from '@material-ui/core';
import LocationIcon from '@material-ui/icons/LocationOn';
import Rating from '@material-ui/lab/Rating';
import axios from 'axios';
import useStyles from './styles.js';

const Map = ({ points, coords, zoom, setSelectedPoint, setSelectedCenterId }) => {
  const classes = useStyles();
  const isMobile = useMediaQuery('(min-width:600px)');


  // 40.3595573680111, -99.45250552767216
  const mapOptions = {
    fullscreenControl: false,
    zoomControl: false
  };
  
  return (
    <div className={classes.mapContainer}>
      <GoogleMapReact
        bootstrapURLKeys={{ key: 'AIzaSyBLZB4eDXdxsrMBGBjTGywYA2n4d85oAIE' }}
        defaultCenter={ coords }
        center={ coords }
        defaultZoom={ zoom }
        margin={[50, 50, 50, 50]}
        options={''}
        onChange={''}
        onChildClick={''}
        options={ mapOptions }
        onClick={() => { setSelectedPoint(null); setSelectedCenterId(null); }}

      >
        { points?.map((point) => (
          <div
            className={classes.markerContainer}
            lat={Number(point.lat)}
            lng={Number(point.lng)}
          >
            {point.studentId == 0 ?
            (
              <LocationIcon className="location" style={{ color: "#EB1C2C" }} onClick={() => { setSelectedCenterId(point.centerId) }}/>
            ):
            (
              <LocationIcon className="location" style={{ color: "blue" }} onClick={() => { setSelectedPoint({ lat: point.lat, lng: point.lng }) }}/>
            )}
          </div>
        ))}
      
      </GoogleMapReact>
    </div>
  );
};

export default Map;