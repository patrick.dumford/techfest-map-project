import './Button.css';
import * as React from 'react';
import { Paper, InputBase, Button } from '@material-ui/core';
import SearchIcon from '@material-ui/icons/Search';


const SearchBar = ({ setModal }) => {

  return(
    <Button variant="contained" material="Paper" className="button"
      onClick={() => 
        setModal( true )
      }
    >
        I want to live here!
    </Button>

  )
};

export default SearchBar;