import './SideCard.css';
import React from 'react';
import axios from 'axios';
import PlacesAutocomplete, {
    geocodeByAddress,
  } from 'react-places-autocomplete';
import { CenterFocusStrong } from '@material-ui/icons';

const StudentSideCard = ({ selectedPoint }) => {
    const [schools, setSchools] = React.useState([]);
    const [center, setCenter] = React.useState({});
    const [student, setStudent] = React.useState({});

    React.useEffect(() => {
        axios
          .get("https://localhost:44326/api/Students/SchoolCount/" + selectedPoint.lat + "/" + selectedPoint.lng)
          .then((res) => {
              console.log("test!!!!");
              console.log(res);

              setSchools(res.data);
          })
          .catch(function (error) {
            console.log(error);
        });

        axios
          .get("https://localhost:44326/api/Points/NearestCenter/" + selectedPoint.lat + "/" + selectedPoint.lng)
          .then((res) => setCenter(res.data))
          .catch(function (error) {
            console.log(error);
        });

        axios
          .get("https://localhost:44326/api/Students/StudentByLocation/" + selectedPoint.lat + "/" + selectedPoint.lng)
          .then((res) => { console.log(res.data); setStudent(res.data); })
          .catch(function (error) {
            console.log(error);
        });
        
      },[]);


    return (
        <div material="Paper" className="side-card">
            <h1 className="card-title">{ student.locationName }</h1>
            <p className="lat-lng">lat: { selectedPoint.lat }, lng: { selectedPoint.lng }</p>

            <div className="table-container">
                <div className="table-item table-header">
                    <div className="school">School</div>
                    <div className="count">#</div>
                </div>
                <div style={{maxHeight: 250, overflowY: 'scroll'}} className="test">
                    { schools?.map((school) => (
                        <div className="table-item">
                            <div className="school">{ school.schoolName }</div>
                            <div className="count">{ school.count }</div>
                        </div>
                    ))}
                </div>
                
            </div>

            <div className="location-container">
                <h2>Nearest RSI location: { center.cityState }</h2>
                <img src={ center.imageUrl } className="img"/>
            </div>
        </div>
    )
}

export default StudentSideCard
