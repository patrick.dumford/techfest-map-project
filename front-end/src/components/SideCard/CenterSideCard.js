import './SideCard.css';
import React from 'react';
import axios from 'axios';
import { Phone } from '@material-ui/icons';

const CenterSideCard = ({ CenterId }) => {
    const [center, setCenter] = React.useState({});

    React.useEffect(() => {
        axios
          .get("https://localhost:44326/api/RsiCenters/" + CenterId)
          .then((res) => {
            setCenter(res.data);
          })
          .catch(function (error) {
            console.log(error);
        }); 
    },[]);


    return (
        <div material="Paper" className="side-card">
            <div className="dev-center">Development Center:</div>
            <h1 className="center-location">{ center.cityState }</h1>
            <p className="lat-lng">lat: { center.lat }, lng: { center.lng }</p>

            <div className="phone-container">
              < Phone className="phone-icon" />
              <h2 className="phone-number">{ center.phoneNumber }</h2>
            </div>

            <div className="location-container">
                <h2>{ center.address }</h2>
                <img src={ center.imageUrl } className="img center-img"/>
            </div>
        </div>
    )
}

export default CenterSideCard
