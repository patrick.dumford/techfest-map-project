import './Modal.css';

import axios from 'axios';
import React, { useEffect } from 'react';
import { Autocomplete } from '@material-ui/lab';
import { Paper, InputBase, TextField, Button } from '@material-ui/core';

const Modal = ({ setModal, addStudent }) => {
    const [school, setSchool] = React.useState("");

    const [schools, setSchools] = React.useState({});

    React.useEffect(() => {
        axios
          .get("https://localhost:44326/api/Schools/")
          .then((res) => setSchools(res.data))
          .catch(function (error) {
            console.log(error);
        });
    
      },[]);

    return (
        <div material="Paper" className="modal-background">
            <div material="Paper" className="modal-container">
                <h1>Enter School Name</h1>

                <Autocomplete
                    disablePortal
                    id="combo-box-demo"
                    options={ schools }
                    sx={{ width: 300 }}
                    renderInput={(params) => <TextField {...params} label="School" />}
                    onChange={(event, value) => setSchool(value)}
                />

                <Button variant="contained" material="Paper" className="modal-button"
                    onClick={() => 
                        setModal( false )
                    }
                >
                    Cancel
                </Button>
                <Button variant="contained" material="Paper" className="modal-button"
                    onClick={() => {
                        console.log("Add Point Clicked!!!!");
                        console.log(school);
                        addStudent( school )
                        setModal( false )
                    }   
                    }
                >
                    Add Point
                </Button>
            </div>   
        </div>
    )
}

export default Modal