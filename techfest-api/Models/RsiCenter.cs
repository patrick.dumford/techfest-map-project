﻿using System;
using System.Collections.Generic;

#nullable disable

namespace techfest_api.Models
{
    public partial class RsiCenter
    {
        public string Name { get; set; }
        public decimal Longitude { get; set; }
        public decimal Latitude { get; set; }
        public string Description { get; set; }
    }
}
