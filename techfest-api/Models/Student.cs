﻿using System;
using System.Collections.Generic;

#nullable disable

namespace techfest_api.Models
{
    public partial class Student
    {
        public string Name { get; set; }
        public string School { get; set; }
        public string Description { get; set; }
        public decimal Longitude { get; set; }
        public decimal Latitude { get; set; }
    }
}
